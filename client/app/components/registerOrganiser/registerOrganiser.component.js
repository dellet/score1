import template from './registerOrganiser.html';
import controller from './registerOrganiser.controller';
import './registerOrganiser.less';

let registerOrganiserComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default registerOrganiserComponent;
