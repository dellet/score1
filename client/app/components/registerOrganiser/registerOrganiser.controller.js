class RegisterOrganiserController {
    constructor($scope, $state, $rootScope) {
        this.name = 'registerOrganiser';
        this.$scope = $scope;
        this.$state = $state;
        this.$scope.orginiserRegister = this.orginiserRegister;
        this.$rootScope = $rootScope;
        this.firebase = $rootScope.firebase;
    }

    orginiserRegister(email, pass, confirmPass) {
        let _this = this;
        _this.$rootScope.isLoading = true;
        //checking to emails

        if (email !== null && pass === confirmPass) {
            this.firebase.auth().createUserWithEmailAndPassword(email, pass)
                .then(function successCallback(response) {
                    let user = firebase.auth().currentUser;
                    //add organiser role
                    firebase.database().ref('userRoles/' + user.uid)
                        .set({role: 'organiser'});
                    // add email to Organisers Emails
                    user.sendEmailVerification();
                    _this.$rootScope.isLoading = false;
                    _this.$rootScope.showSimpleToast('Organiser register was successfully');

                }, function errorCallback(response) {
                    _this.$rootScope.isLoading = false;
                    _this.$rootScope.showSimpleToast(response.message);
                });
        }
    }

}
RegisterOrganiserController.$inject = ['$scope', '$state', '$rootScope'];
export default RegisterOrganiserController;
