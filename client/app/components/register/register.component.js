import template from './register.html';
import controller from './register.controller';
import './register.less';

let registerComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default registerComponent;
