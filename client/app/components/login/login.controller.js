class LoginController {
    constructor($scope, $rootScope, $state, $mdToast, $http) {
        this.name = 'login';
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.firebase = $rootScope.firebase;
        this.$scope.userLogin = this.userLogin.bind(this);
        this.$state = $state;
        this.$scope.role = +$state.params.role;
        this.$scope.userLoginUsingFacebook = this.userLoginUsingFacebook.bind(this);
        this.$http = $http;
    }


    userLoginUsingFacebook() {
        let $state = this.$state;
        let _this = this;

        if ($state.organiser === 1) {
            return
        }

        let provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function (result) {
                let user = result.user;
                _this.$rootScope.currentUser = user;
                console.log(user.email);
                let role = +$state.params.role;
                if (role === 0) {
                    _this.$rootScope.currentRole = 'athlete';
                    localStorage.setItem('currentRole', 'athlete');
                    $state.go('requireAuth.userProfile');
                } else if (role === 1) {
                    _this.$rootScope.currentRole = 'organiser';
                    localStorage.setItem('currentRole', 'organiser');
                    $state.go('requireAuth.organiserProfile');
                }

            }).catch(function (error) {

            // let errorMessage = error.message;
            // console.log(errorMessage);
            _this.$rootScope.showSimpleToast(res.message);
        });
    };

    userLogin(email, pass) {
        let $state = this.$state;
        let _this = this;
        _this.$rootScope.isLoading = true;

        if (email !== null && pass !== null) {
            this.firebase.auth().signInWithEmailAndPassword(email, pass)
                .then(function successCallback(user) {
                    _this.$rootScope.isLoading = false;
                    _this.$rootScope.currentUser = user;
                    if (user.emailVerified) {
                        let role = +$state.params.role;
                        if (role === 0) {
                            _this.$rootScope.currentRole = 'athlete';
                            localStorage.setItem('currentRole', 'athlete');
                            $state.go('requireAuth.userProfile');
                        } else if (role === 1) {
                            _this.$rootScope.currentRole = 'organiser';
                            localStorage.setItem('currentRole', 'organiser');
                            $state.go('requireAuth.organiserProfile');
                        }
                    }
                    else {
                        _this.$rootScope.showSimpleToast('User is unverified, please check your email: ' + user.email + ' for confirmation');
                    }

                }, function errorCallback(res) {
                    _this.$rootScope.isLoading = false;
                    _this.$rootScope.showSimpleToast(res.message);
                })
        } else {
            _this.$rootScope.isLoading = false;
            _this.$rootScope.showSimpleToast('Check please inputed data');
        }
    };

}

LoginController.$inject = ['$scope', '$rootScope', '$state', '$mdToast', '$http'];

export default LoginController;
