class UserProfileController {
    constructor($scope, $rootScope, $firebaseAuth, $q, $http, Upload) {
        this.name = 'userProfile';
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.firebase = $rootScope.firebase;
        this.$firebaseAuth = $firebaseAuth;
        this.getUserInfo.apply(this);
        this.$scope.saveUserInfo = this.saveUserInfo.bind(this);
        this.$scope.userBirthday = this.userBirthday;
        this.$scope.maxDateForDatePicker = new Date();
        this.$scope.dataForDataPicer = this.dtaForDataPicer;
        this.$http = $http;
        this.Upload = Upload;
        this.watchers();
        this.initDate();
    }

    initDate() {
        let date = new Date(),
            year = new Date().getFullYear();

        this.$scope.maxDate = new Date(date.setFullYear(year - 18))
    }


    getUserInfo() {
        let _this = this;
        _this.$rootScope.isLoading = true;
        _this.firebaseUser = this.$firebaseAuth().$getAuth();
        if (!_this.firebaseUser) {
            _this.$rootScope.showSimpleToast('Sorry, user not found');
            return
        }

        firebase.database().ref('users/' + this.firebaseUser.uid).once('value').then(function (user) {
            if (user.val() && user.val().birthdate) {
                _this.$scope.userBirthday = new Date(user.val().birthdate)
            }
            _this.$rootScope.isLoading = false;
            _this.$scope.user = user.val();
            setTimeout(()=> {
                _this.$scope.$apply();
            });

        }, (err)=> {
            _this.$rootScope.isLoading = false;
            _this.$rootScope.showSimpleToast('Sorry, user not found')
        });
    }

    saveUserInfo(user) {
        let _this = this;
        _this.$rootScope.isLoading = true;
        if (_this.$scope.userBirthday) {
            user.birthdate = _this.$scope.userBirthday.toDateString();
        }
        if (_this.$scope.picFile) {
            user.picFile = _this.$scope.userBirthday.toDateString();
        }
        if (!this.firebaseUser) {
            _this.$rootScope.isLoading = false;
            _this.$rootScope.showSimpleToast("This user not exists");
            return
        }
        firebase.database().ref('users/' + this.firebaseUser.uid).set(user).then(function successCallback(res) {
            _this.$rootScope.showSimpleToast("Changes saved successfully");
            _this.$rootScope.isLoading = false;
        }, function errorCallback(res) {
            _this.$rootScope.showSimpleToast(res.message);
            _this.$rootScope.isLoading = false;
        });
    }

    watchers() {
        this.$scope.$watch('picPhoto', (newVal, oldVal, $scope)=> {
            if (newVal) {
                this.Upload.base64DataUrl(newVal).then(function (base64Urls) {
                    if (!$scope.user) {
                        $scope.user = {}
                    }
                    $scope.user.picPhoto = base64Urls;
                });
            }

        })
    }
}

UserProfileController.$inject = ['$scope', '$rootScope', '$firebaseAuth', '$q', '$http', 'Upload'];

export default UserProfileController;
