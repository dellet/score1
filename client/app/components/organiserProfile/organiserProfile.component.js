import template from './organiserProfile.html';
import controller from './organiserProfile.controller';
import './organiserProfile.less';

let organiserProfileComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default organiserProfileComponent;
