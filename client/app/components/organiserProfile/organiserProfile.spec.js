import OrganiserProfileModule from './organiserProfile'
import OrganiserProfileController from './organiserProfile.controller';
import OrganiserProfileComponent from './organiserProfile.component';
import OrganiserProfileTemplate from './organiserProfile.html';

describe('OrganiserProfile', () => {
    let $rootScope, makeController;

    beforeEach(window.module(OrganiserProfileModule));
    beforeEach(inject((_$rootScope_) => {
        $rootScope = _$rootScope_;
        makeController = () => {
            return new OrganiserProfileController();
        };
    }));

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    describe('Controller', () => {
        // controller specs
        it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
            let controller = makeController();
            expect(controller).to.have.property('name');
        });
    });

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(OrganiserProfileTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
        });
    });

    describe('Component', () => {
        // component/directive specs
        let component = OrganiserProfileComponent;

        it('includes the intended template', () => {
            expect(component.template).to.equal(OrganiserProfileTemplate);
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(OrganiserProfileController);
        });
    });
});
