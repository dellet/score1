class OrganiserProfileController {
    constructor($scope, firebase, $rootScope, Upload, $firebaseAuth, $firebaseArray, $state) {
        this.name = 'organiserProfile';
        this.init($scope, firebase, $rootScope, Upload, $firebaseAuth, $firebaseArray, $state);
        this.getEvents();
        this.watchers();
        this.getUserInfo();

    }

    init($scope, firebase, $rootScope, Upload, $firebaseAuth, $firebaseArray, $state) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.firebase = firebase;
        this.$firebaseArray = $firebaseArray;
        this.$scope.createEvent = this.createEvent;
        this.$scope.$state = $state;
        this.$scope.getEvents = this.getEvents;
        this.$scope.saveUserInfo = this.saveUserInfo.bind(this);
        this.$firebaseAuth = $firebaseAuth;
        this.Upload = Upload;
        this.$scope.query = {
            order: "timestamp",
            limit: 10,
            skip: 0,
            page: 1
        };

        this.$scope.loadEvents = this.loadEvents;
    }

    getEvents() {
        //let timestamp = new Date().getTime().toString();
        let userID = firebase.auth().currentUser.uid,
            data = [],
            userData = [],
            _this = this,
            ref = firebase.database().ref('events/');

        ref.orderByChild(_this.$scope.query.order)
            .limitToLast(_this.$scope.query.limit * (_this.$scope.query.page || 1))
            //.startAt(_this.$scope.query.skip)
            .once('value')
            .then(function (snapshot) {
                _this.$scope.eventsData = snapshot.val();
            });


    }

    // createEvent(){
    //   let timestamp = new Date().getTime();
    //   let user= firebase.auth().currentUser;
    //   let objectt = {
    //         name: "third event",
    //         description: "test for Event",
    //         date: new Date().toJSON(),
    //         userId: user.uid,
    //         timestamp: -timestamp
    //   };

    //   let ref = firebase.database().ref('events/');
    //   let item = ref.push();
    //   item.setWithPriority(objectt, 0 - Date.now());
    // }


    // loadEvents(){
    //   let _this = this;
    //   let query = _this.$scope.query;
    //   firebase.database().ref('events')
    //     .orderByChild(query.order)
    //     .startAt(query.skip)
    //     //.limitToFirst(query.limit)
    //     .on('value', (res) => {
    //
    //       _this.$scope.events = res.val();
    //       setTimeout(()=>{
    //         _this.$scope.$apply()
    //       },1);
    //
    //   }, (err) => {
    //     _this.$rootScope.showSimpleToast(err.message);
    //   });
    //
    // }

    watchers() {
        this.$scope.$watch('picPhoto', (newVal, oldVal, $scope)=> {
            if (newVal) {
                this.Upload.base64DataUrl(newVal).then(function (base64Urls) {
                    if (!$scope.organiser) {
                        $scope.organiser = {}
                    }
                    $scope.organiser.picPhoto = base64Urls;
                });
            }

        })
    }


    getUserInfo() {
        let _this = this;
        _this.$rootScope.isLoading = true;
        _this.firebaseUser = this.$firebaseAuth().$getAuth();
        if (!_this.firebaseUser) {
            _this.$rootScope.showSimpleToast('Sorry, user not found');
            return
        }

        firebase.database().ref('organisers/' + this.firebaseUser.uid).once('value').then(function (user) {
            _this.$rootScope.isLoading = false;
            _this.$scope.organiser = user.val();
            setTimeout(()=> {
                _this.$scope.$apply();
            });

        }, (err)=> {
            _this.$rootScope.isLoading = false;
            _this.$rootScope.showSimpleToast('Sorry, user not found')
        });
    }


    saveUserInfo(user) {
        let _this = this;
        _this.firebaseUser = this.$firebaseAuth().$getAuth();
        _this.$rootScope.isLoading = true;

        if (_this.$scope.picFile) {
            user.picFile = _this.$scope.userBirthday.toDateString();
        }
        if (!_this.firebaseUser) {
            _this.$rootScope.isLoading = false;
            _this.$rootScope.showSimpleToast("This user not exists");
            return
        }
        firebase.database().ref('organisers/' + _this.firebaseUser.uid).set(user).then(function successCallback(res) {
            _this.$rootScope.showSimpleToast("Changes saved successfully");
            _this.$rootScope.isLoading = false;
        }, function errorCallback(res) {
            _this.$rootScope.showSimpleToast(res.message);
            _this.$rootScope.isLoading = false;
        });
    }

}

OrganiserProfileController.$inject = ['$scope', 'firebase', '$rootScope', 'Upload', '$firebaseAuth', '$firebaseArray', '$state'];

export default OrganiserProfileController;
