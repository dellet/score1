import angular from 'angular';
import uiRouter from 'angular-ui-router';
import organiserProfileComponent from './organiserProfile.component';

let organiserProfileModule = angular.module('organiserProfile', [
    uiRouter
])

    .component('organiserProfile', organiserProfileComponent)

    .name;

export default organiserProfileModule;
