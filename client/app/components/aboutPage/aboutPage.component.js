import template from './aboutPage.html';
import controller from './aboutPage.controller';
import './aboutPage.less';

let aboutPageComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default aboutPageComponent;
