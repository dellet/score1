import angular from 'angular';
import uiRouter from 'angular-ui-router';
import aboutPageComponent from './aboutPage.component';

let aboutPageModule = angular.module('aboutPage', [
    uiRouter
])

    .component('aboutPage', aboutPageComponent)

    .name;

export default aboutPageModule;
