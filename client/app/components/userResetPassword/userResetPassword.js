import angular from 'angular';
import uiRouter from 'angular-ui-router';
import userResetPasswordComponent from './userResetPassword.component';

let userResetPasswordModule = angular.module('userResetPassword', [
    uiRouter
])

    .component('userResetPassword', userResetPasswordComponent)

    .name;

export default userResetPasswordModule;
