import template from './userResetPassword.html';
import controller from './userResetPassword.controller';
import './userResetPassword.less';

let userResetPasswordComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default userResetPasswordComponent;
