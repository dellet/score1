import template from './createEvent.html';
import controller from './createEvent.controller';
import './createEvent.less';

let createEventComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default createEventComponent;
