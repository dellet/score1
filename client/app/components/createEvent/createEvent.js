import angular from 'angular';
import uiRouter from 'angular-ui-router';
import createEventComponent from './createEvent.component';

let createEventModule = angular.module('createEvent', [
    uiRouter
])

    .component('createEvent', createEventComponent)

    .name;

export default createEventModule;
