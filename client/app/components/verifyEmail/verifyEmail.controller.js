class VerifyEmailController {
    constructor($scope, $rootScope, $state, $location, $mdToast, $timeout) {
        this.name = 'verifyEmail';
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$timeout = $timeout;
        this.$location = $location;
        this.$scope.verifyUserEmail = this.verifyUserEmail.bind(this);

    }

    verifyUserEmail() {
        let _this = this;
        //get url like object
        let url = this.$location.search();
        let auth = firebase.auth();
        if (url.oobCode && url.mode && url.apiKey) {
            _this.$rootScope.urlIsGood = true;
            auth.applyActionCode(url.oobCode).then(function (res) {
                console.log("User Email was successfuly verified");
                _this.$timeout(function () {
                    _this.$state.go('home')
                }, 3000);
                _this.$rootScope.showSimpleToast('User Email was successfuly verified');
            }).catch(function (error) {
                _this.$rootScope.urlIsGood = false;
                console.log(error.message);
                //_this.$rootScope.showSimpleToast(error.message);
                _this.$timeout(function () {
                    _this.$state.go('home')
                }, 3000);
            });
        }
        else {
            _this.$rootScope.urlIsGood = false;
        }

    }
}

VerifyEmailController.$inject = ['$scope', '$rootScope', '$state', '$location', '$mdToast', '$timeout'];
export default VerifyEmailController;
