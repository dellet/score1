import VerifyEmailModule from './verifyEmail'
import VerifyEmailController from './verifyEmail.controller';
import VerifyEmailComponent from './verifyEmail.component';
import VerifyEmailTemplate from './verifyEmail.html';

describe('VerifyEmail', () => {
    let $rootScope, makeController;

    beforeEach(window.module(VerifyEmailModule));
    beforeEach(inject((_$rootScope_) => {
        $rootScope = _$rootScope_;
        makeController = () => {
            return new VerifyEmailController();
        };
    }));

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    describe('Controller', () => {
        // controller specs
        it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
            let controller = makeController();
            expect(controller).to.have.property('name');
        });
    });

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(VerifyEmailTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
        });
    });

    describe('Component', () => {
        // component/directive specs
        let component = VerifyEmailComponent;

        it('includes the intended template', () => {
            expect(component.template).to.equal(VerifyEmailTemplate);
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(VerifyEmailController);
        });
    });
});
