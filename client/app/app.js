import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import 'normalize.css';
import 'angular-animate';
import 'angular-aria';
import 'angular-material';
import 'angular-mocks';
import 'angular-material/angular-material.min.css';
import 'angularfire'
import 'angular-material-data-table/dist/md-data-table'
import 'angular-material-data-table/dist/md-data-table.css'
import 'ng-file-upload/dist/ng-file-upload'
import 'ng-file-upload/dist/ng-file-upload-shim'

angular.module('app', [
    uiRouter,
    Common,
    Components,
    'ngMaterial',
    'firebase',
    'md.data.table',
    'ngFileUpload'
])
    .config(($locationProvider) => {
        "ngInject";
        // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
        // #how-to-configure-your-server-to-work-with-html5mode
        $locationProvider.html5Mode(true).hashPrefix('!');
    })
    .config(($stateProvider, $urlRouterProvider) => {
        "ngInject";

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('home', {
                url: '/',
                component: 'home',
                data: {
                    pageTitle: "Home"
                }
            })
            .state('auth', {
                template: '<ui-view></ui-view>'
            })
            .state('requireAuth', {
                template: '<ui-view></ui-view>'
            })
            .state('auth.login', {
                url: '/login',
                component: 'login',
                params: {
                    role: ""
                },
                data: {
                    pageTitle: "Login"
                }
            })
            .state('auth.register', {
                url: '/register',
                component: 'register',
                params: {
                    role: "",
                },
                data: {
                    pageTitle: "Registration"
                }

            })
            .state('requireAuth.userProfile', {
                url: '/userProfile',
                component: 'userProfile',
                data: {
                    pageTitle: "Athletes"
                },
                params: {
                    role: ""
                }
            })
            .state('auth.userResetPassword', {
                url: '/userResetPassword',
                component: 'userResetPassword',
                params: {
                    organiser: 0,
                },
                data: {
                    pageTitle: "Reset Password"
                }
            })
            .state('requireAuth.organiserProfile', {
                url: '/organiserProfile',
                component: 'organiserProfile',
                data: {
                    pageTitle: "Organisers"
                },
                params: {
                    role: ""
                }
            })

            .state('eventPage', {
                url: '/eventPage/:id',
                component: 'eventPage',
                data: {
                    pageTitle: "Events"
                }
            })
            .state('aboutPage', {
                url: '/aboutPage',
                component: 'aboutPage',
                data: {
                    pageTitle: "About"
                }
            })
            .state('verifyEmail', {
                url: '/verifyEmail',
                component: 'verifyEmail',
                data: {
                    pageTitle: "Verify Email"
                }
            })
            .state('createEvent', {
                url: '/createEvent',
                component: 'createEvent',
                data: {
                    pageTitle: 'Create Event'
                }
            })
    })
    .run([
        '$rootScope',
        'firebase',
        '$mdToast',
        '$firebaseObject',
        '$firebaseAuth',
        '$state',
        '$transitions',
        '$timeout',
        '$q',
        '$http',
        ($rootScope, firebase, $mdToast, $firebaseObject, $firebaseAuth, $state, $transitions, $timeout, $q, $http)=> {
            let authObject = $firebaseAuth();

            $rootScope.isLoggedIn = authObject.$getAuth() && authObject.$getAuth().uid;
            $rootScope.firebase = firebase;
            $rootScope.$mdToast = $mdToast;
            $rootScope.$firebaseObject = $firebaseObject;
            //check value of good\bad url at verify emailVerified
            $rootScope.urlIsGood = true;

            /**
             * Auth control by transitions
             * */

            $transitions.onBefore({to: 'requireAuth.**'}, (trans)=> {
                let currentUser = $rootScope.currentUser,
                    target = trans.router.stateService.target,
                    componentToName,
                    userRole = $rootScope.currentRole;

                try {
                    componentToName = trans.to().component
                } catch (e) {
                    console.log(e)
                }

                if (currentUser && currentUser.emailVerified) {
                    if (componentToName) {
                        switch (trans.to().component) {
                            case "userProfile":
                                if (userRole === 'organiser') {
                                    $rootScope.showSimpleToast('You must be logged in as a athlete');
                                    return target(trans.from().name || 'home');
                                }
                                break;
                            case "organiserProfile":
                                if (userRole === 'athlete') {
                                    $rootScope.showSimpleToast('You must be logged in as a organiser');
                                    return target(trans.from().name || 'home');
                                }
                                break;
                            default :
                                return 1;
                        }
                    }
                } else {
                    if (componentToName) {
                        switch (trans.to().component) {
                            case "userProfile":
                                return target('auth.login', {role: 0});
                                break;
                            case "organiserProfile":
                                return target('auth.login', {role: 1});
                                break;
                            default :
                                return target('auth.login');

                        }
                    } else {
                        return target('auth.login');
                    }
                }
            });

            $transitions.onBefore({to: 'auth.**'}, (trans)=> {
                let currentUser = $rootScope.currentUser,
                    target = trans.router.stateService.target;

                if (currentUser && currentUser.emailVerified) {
                    return target(trans.from().name || 'home')
                } else {
                    return 1
                }
            });

            $transitions.onSuccess({to: '**'}, (trans)=> {
                $rootScope.title = ($state.current.data && $state.current.data.pageTitle)
                    ? $state.current.data.pageTitle
                    : 'Default title';
            });

            $rootScope.showSimpleToast = (message)=> {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(message)
                        .hideDelay(3000)
                        .position('center right')
                );
            };

            $rootScope.querySearchCountry = (searchValue) => {
                if (!searchValue) {
                    return
                }

                let deffer = $q.defer();

                return $http({
                    method: "GET",
                    url: "https://restcountries.eu/rest/v1/name/" + searchValue
                }).then((data)=> {
                    //return data;
                    deffer.resolve(data.data);
                    return deffer.promise
                });
            };


            /**
             * Watchers
             */
            $rootScope.$watch('currentUser', (newVal, oldVal, $rootScope)=> {
                /**
                 * Getting current user with it role to rootScope
                 */
                if (!newVal) {
                    $rootScope.currentUser = authObject.$getAuth();
                }
            });

            $rootScope.$watch('currentRole', (newVal, oldVal, $rootScope)=> {
                /**
                 * Getting current user with it role to rootScope
                 */
                if (!newVal) {
                    $rootScope.currentRole = localStorage.getItem('currentRole')
                }
            })

        }])

    .component('app', AppComponent);
