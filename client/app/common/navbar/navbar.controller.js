class NavbarController {
    constructor($scope, $rootScope, $state, $firebaseAuth) {
        this.name = 'navbar';
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$firebaseAuth = $firebaseAuth;
        this.logout = this.logoutFunc.bind(this)
    }

    logoutFunc() {
        let that = this;
        that.$rootScope.isLoading = true;
        this.$firebaseAuth().$signOut().then(()=> {
            that.$rootScope.isLoading = false;
            localStorage.removeItem('currentRole');
            that.$rootScope.currentUser = null;
            that.$state.go('home');
        })
    };
}
NavbarController.$inject = ['$scope', '$rootScope', '$state', '$firebaseAuth'];
export default NavbarController;
